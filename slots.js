/*defining constants for imgs, statuses, this particular slot machine settings*/
const IMG_SLOT = "img/rotation.jpg";
const IMG_BUTTON = "img/start.jpg";

const STATUS_ZERO = 0;
const STATUS_INIT = 1;
const STATUS_MOVING = 2;
const STATUS_CHECK_WIN = 3;

const SLOT_NUMBER = 3;
const INITIAL_X = 25;
const INITIAL_Y = 15;
const SLOT_FRAME_SIZE = 100;
const ROTATES_QTY = 2;
const PIECES = 4;
const DELAY = 5;

let speeds = [],
    initialSpeed = 15,
    status = 0,
    finalFrameY = [],
    slotSprite = [],
    initPosition = [];

/*intialising pixi application*/

let slots = new PIXI.Application( window.innerWidth, window.innerHeight, {backgroundColor: 0x000000});

document.body.appendChild(slots.view);
slots.interactive = true;

/*loading imgs*/

let loader = PIXI.loader;

loader.add('machine', IMG_SLOT);
loader.add('button', IMG_BUTTON);

loader.once('complete', init);
loader.load();

/*initialising the slots*/

function init() {
    let texture = PIXI.TextureCache[IMG_BUTTON],
        buttonSprite = new PIXI.Sprite(texture);
    buttonSprite.x = 15;
    buttonSprite.y = 150;
    slots.stage.addChild(buttonSprite);
    buttonSprite.interactive = true;
    buttonSprite.click = function () {
        startAnimation();
    };

    texture = PIXI.TextureCache[IMG_SLOT];
    initPosition = [];

    for (let i = 1; i <= SLOT_NUMBER; i++) {
        initPosition.push(i);
    }

    for (let i=0; i < SLOT_NUMBER; i++) {
        slotSprite[i] = new PIXI.TilingSprite(texture, SLOT_FRAME_SIZE, SLOT_FRAME_SIZE);
        slotSprite[i].tilePosition.x = 0;
        slotSprite[i].tilePosition.y = (-initPosition[i]*SLOT_FRAME_SIZE);
        slotSprite[i].x = INITIAL_X +(i*190);
        slotSprite[i].y = INITIAL_Y;
        slots.stage.addChild(slotSprite[i]);
    }
    draw();
}

/*speed delay for spining reels*/

for (let i = 0; i < SLOT_NUMBER; i++) {
    speeds.push(initialSpeed + DELAY * i);
}

/*drawing the slots*/

function draw() {
    if (status==STATUS_ZERO) {
        status=STATUS_INIT;
    } else if (status==STATUS_INIT) {
        status=STATUS_CHECK_WIN;
    } else if (status==STATUS_MOVING) {

        for (let i = 0; i < SLOT_NUMBER; i++) {
            if (finalFrameY[i] > 0) {
                slotSprite[i].tilePosition.y = slotSprite[i].tilePosition.y + speeds[i];
                finalFrameY[i]= finalFrameY[i] - speeds[i];
            }
        }

        if (finalFrameY[0] <= 0) {
            status=STATUS_CHECK_WIN;
        }

    } else if (status === STATUS_CHECK_WIN) {
        let check = true;
        console.warn(0, finalFrameY[0]);
        for (let i = 1; i < SLOT_NUMBER; i++) {
            if (initPosition[i] !== initPosition[i-1]) {
                check = false;
            }

            console.warn(i, finalFrameY[i]);
        }

        if (check) {
            alert("WIN!");
        }
        return;
    }
    slots.render(slots);
    requestAnimationFrame(draw);
}

/*launching animation*/

function startAnimation() {
    if( status==STATUS_INIT || status==STATUS_CHECK_WIN ) {
        initPosition = getPositions();

        for (let i = 0; i < SLOT_NUMBER; i++) {
            slotSprite[i].tilePosition.y = (-initPosition[i] * SLOT_FRAME_SIZE);
            finalFrameY[i] = (ROTATES_QTY * SLOT_FRAME_SIZE * PIECES);
        }
        status = STATUS_MOVING;
        draw();
    }
}


function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*position logic - made it simple random */

function getPositions() {
    let positions = [];

    for (let i = 0; i < SLOT_NUMBER; i++) {
        positions.push(getRandom(0,SLOT_NUMBER - 1));
    }

    return positions;
}
